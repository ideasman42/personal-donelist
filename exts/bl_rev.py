from docutils.nodes import reference
from docutils.utils import unescape
from docutils.parsers.rst.roles import set_classes

CHECK_LOCAL_SHA1 = True

def bl_rev_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    if " " in text:
        msg = inliner.reporter.error(
            '"rev" role expected a sha1; '
            '"%s" is invalid.' % text, line=lineno)
        prb = inliner.problematic(rawtext, rawtext, msg)
        return [prb], [msg]

    app = inliner.document.settings.env.app
    node = make_link_node(rawtext, app, text, options)
    return [node], []


def make_link_node(rawtext, app, slug, options):
    try:
        base = app.config.bl_rev_bases
        if not base:
            raise AttributeError
    except AttributeError as err:
        raise ValueError('"bl_rev_bases" configuration value is not set (%s)' % str(err))

    for rev_key, (base_local, base_remote) in base.items():
        if slug.startswith(rev_key):
            break
    else:
        raise Exception("Slug prefix not found for %r" % slug)

    slug = slug[len(rev_key):]

    if CHECK_LOCAL_SHA1:
        if base_local is not None:
            import os
            if os.path.isdir(base_local):
                import subprocess
                try:
                    subprocess.check_output(
                        ["git", "rev-parse", "--quiet", "--verify", slug],
                        cwd=base_local,
                    )
                except subprocess.CalledProcessError:
                    # could make a warning
                    raise Exception("Missing revision: %r does not contain %r" % (base_local, slug))
            else:
                print("Local path missing:", base_local)

    slash = '/' if base_remote[-1] != '/' else ''
    ref = base_remote + slash + slug

    set_classes(options)
    node = reference(
            rawtext,
            # text to draw
            # shorten sha1 for display
            rev_key + ":" + unescape(slug[:8]),
            # url to link to
            refuri=ref,
            **options)
    return node


def setup(app):
    app.add_role('rev', bl_rev_role)
    try:
        app.add_config_value('bl_rev_bases', None, 'env')
    except:
        pass
    return
