
.. TEMPLATE
   
   Week XX (Month XX)
   ==================
   
   Info
   ----
   
   
   Next Week
   ---------
   
   
   General Tasks
   -------------
   
   - 
   
   BAM
   ^^^
   
   
   Documentation
   ^^^^^^^^^^^^^
   
   
   fixes (unreported)
   ------------------
   
   - 
   
   
   fixes (reported)
   ----------------
   
   - 
   
   
   patches
   -------
   
   - 
   
   
   code-review
   -----------
   
   - 
   
   Fixed: 0, Patches: 0
   Unreported-Fixed 0
   
   up until :rev:`B`, :rev:`BA`


