
SPHINXOPTS    =
PAPER         =
SPHINXBUILD   = sphinx-build
BUILDDIR      = build

all: FORCE
	$(SPHINXBUILD) -b html $(SPHINXOPTS) ./content "$(BUILDDIR)/html"
	@echo " xdg-open \"$(BUILDDIR)/html/index.html\""

clean: FORCE
	rm -rf "$(BUILDDIR)/html" "$(BUILDDIR)/latex"

upload: all FORCE
	# -x to exclude, copy build/html -> donelist
	@lftp ftp://$(FTP_USER):$(FTP_PWD)@$(FTP_HOST) -e "mirror -x .doctrees --delete -R ./build/html ./donelist ; quit"

FORCE:
