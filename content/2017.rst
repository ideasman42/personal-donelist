
****
2017
****

..
   Week 2 (August 15)
   ==================

   Info
   ----


   Next Week
   ---------


   General Tasks
   -------------

   -
     :rev:`RE`

   BMesh-Rust
   ----------

   -
     :rev:`RB`


   Questions
   ---------


   .. hlist::
      :columns: 1

      - :qa:``


Week 29 (February 20)
=====================

Info
----

This week I got SDNA encoding/decoding so DNA definitions are written into the file and read back.

Currently this data isn't use for file reading, supporting reading old DNA into new DNA is another task.


Next Week
---------

I've moved to working on other projects so wont be updating this page for the foreseeable future.

I've made a post `Trying out Rust for Graphics Programming (experience after 7 months)
<https://www.reddit.com/r/rust/comments/5woap4/trying_out_rust_for_graphics_programming>`__
which sums up my experience.

BMesh-Rust
----------

- SDNA: working SDNA decoding
  :rev:`RBbcfd574789957453b7dfa92aef351678e5da2e61`


Questions
---------

.. hlist::
   :columns: 1

   - :qa:`42406538` How to access a slice from pre-defined 'Range' in a struct?
   - :qa:`42406538` File or module level 'feature' possible?
   - :qa:`42344057` How to detect when calling 'drop' is needed for a type?


Week 28 (February 13)
=====================

Info
----

This week I worked entirely on reading back the file-format into memory,
since this is a structured memory dump, it's quite involved to ensure data is correctly encoded and decoded.

Currently reading and writing works, but support for changes to the format aren't yet implemented
(so changes to structs wont break loading).

When reading binary data into memory I ran into a surprising number of problems that took time to resolve.

Next Week
---------

Add support for saving and loading the struct definitions,
where members are added and removed.


BMesh-Rust
----------

- DNA: Initial de-serializing API
  :rev:`RB333497aa661d24e301fb00a9c2ff4cc85d5be141`
- DNA: various encoding fixes
  :rev:`RB721a821bd019d579cce2f2f17d2acc105dcfdfb8`
- DNA: move non-ID data to generic mempool store
  :rev:`RB80ff6e144eac7a4bcddd2c2014742b65aea80093`
- DNA: Add ``MemPoolSeq.iter_mut()`` function
  :rev:`RB825d2133514a5539c1712b40eabf958053abcdfc`
- DNA: split MainData into modules
  :rev:`RBe84abe43f09811f06313f672363f886ac7ea6b35`
- MemPool: add default size to trait.

  This simplifies mempool creation and allows ``Default`` trait use.
  :rev:`RBf53dbf21aba3c032091a9160dd63a6d02e216b12`
- DNA: manual drop, needed for memory pool use
  :rev:`RB340dab0f04e84464f9477cef9b4c58f23635bbd1`

Questions
---------

.. hlist::
   :columns: 1

   - :qa:`42322879` How to remove Rust compiler toolchains with Rustup?
   - :qa:`42290414` How to use ``Read::take`` recursively to read a tree structures with a generic Read trait?
   - :qa:`42277783` How to find the location of a macro's use between crates?
   - :qa:`42275777` How to trace the cause of an error result?
   - :qa:`42273673` How to format strings with indentation based on an integer?
   - :qa:`42243355` How to advance through data from the ``std::io::Read`` trait when ``Seek`` isn't implemented?
   - :qa:`42241233` Is there any practical considerations to prefer one notation for converting vectors into slices?
   - :qa:`42240663` How to read (``std::io::Read``) from a Vec or Slice?
   - :qa:`42217310` How to depend on a crate, only for tests?
   - :qa:`42199727` How to construct const integers from literal byte expressions?
   - :qa:`42199312` Is there a byte equivalent of the ``stringify`` macro?
   - :qa:`42198466` How to warn/error when a struct is padded?
   - :qa:`42187591` How to keep track of how many bytes written when using ``std::io::Write`` ?


Up until :rev:`RB333497aa661d24e301fb00a9c2ff4cc85d5be141`


Week 27 (February 6)
====================

Info
----

This week basic object tools were improved, (adding, deleting & selecting),
added primitive string handling crate to use fixed sized ``[u8]`` arrays as strings.

And spent days preparing for DNA io (serializing and reading back data into memory),
this relies on more involved traits and macros then I've used so far,
ended up asking a lot of questions about more advanced macro use.

Currently the structure is read from DNA types, but writing to files isn't yet implemented.


Next Week
---------

Focus on file IO, so DNA can be written and read.


BMesh-Rust
----------

- DNA: initial serializing API
  :rev:`RB492c92d8379d37b260cd37425270dcb0cb9d7f30`
- Query Macros: add argument counting
  :rev:`RB0aa2c0acbded49683d573aa5e333e9a6690c423b`
- Type Macros: Add ``type_id_of``
  :rev:`RB6be4c049c2c1616dfcdec1e2f163146dd696eee4`
- Pointer Macros: ``size_of``
  :rev:`RBd620c0cd646eea274aced0713f2ab76950f0d6b9`
- Use methods for object-instance flags
  *(More convenient, no need to access name-space of flags all over)*
  :rev:`RB6666c44de7e680501e4cfa12a7b7bf145851c26e`
- Optional methods for flag access
  :rev:`RB18bb8a34904d4af951d0acaaa82cf09c6c153c19`
- Add Empty objects & draw selection in passes
  :rev:`RBe974fbd2787e9272d6dfc6b259923ebc0b1cad0c`
- ``byte_str_util``: initial tests
  :rev:`RBb22d69b1f98d3868c37b7d5b44121fa46c5445f5`
- DNA: ensure unique names when adding new data-blocks
  :rev:`RB63bfce9ca24a5ed61e9e6f4401405727300ae387`
- DNA: implement default_name for ID subtypes
  :rev:`RB3bcd58e81b411e372780646fa3f916265c1ddb18`
- Support adding meshes in object mode
  :rev:`RB5d30d48bec03075ece5ab6b51406dad4eb87654e`
- Add ``byte_str_util`` crate for byte/string util functions.
  *(for handling fixed sized null terminated byte buffers)*
  :rev:`RB4c7457ae0b5e3df9d62f23840f042bbb592befe5`
- Use RNA properties for add primitives
  :rev:`RBbac6fe37fd7481cc1dc0723168aa018f0d2935a1`
- Add ``BMeshPartialGeom.elem_hflag_edit_builder``, Use for for selecting newly created data.
  :rev:`RB2b616e1933cb28c4bde61f590896ebfddab3f012`
- BMesh: move builder into own module
  :rev:`RBc97e30df52aa48fe21e6da8f1ed5178b6b432f96`
- Add object delete operator
  :rev:`RB1f9cb2f315b363e9506192f90ccfef1af3a2520b`
- Check operator poll function before running
  :rev:`RB1a8886deb0a084632f0861ba1420f9d72569429e`
- Object select operators
  :rev:`RBbe36fb17be5541e675c996efda9050f34ea2aa1a`


Questions
---------

.. hlist::
   :columns: 1

   - :qa:`42171483` How to recursively take the last argument of a macro?
   - :qa:`42171160` Using argument number in macro expansion?
   - :qa:`42160773` Is there a way to write to a memory buffer using std::io::Write without re-allocation?
   - :qa:`42134874` Are there equivalents to slice::chunks/windows for iterators to loop over pairs, triplets etc?
   - :qa:`42111590` Possible to access the 'TypeId' of a struct member?
   - :qa:`42107104` How to pass a macro containing multiple items into a macro?
   - :qa:`42105580` How to ensure structs implement functions consistently,
     without callers having to explicitly 'use' the trait?
   - :qa:`42104247` Possible to apply constraints on identifiers passed into a macro?
   - :qa:`42066381` How to get a ``&str`` from a NUL-terminated byte slice
     if the NUL terminator isn't at the end of the slice?
   - :qa:`42063162` Efficient truncating string copy ``str`` to ``[u8]`` (utf8 aware ``strlcpy``)?
   - :qa:`42060310` Setting alignment in source-data for optimal loading into memory?
   - :qa:`42171483` How to recursively take the last argument of a macro?
   - :qa:`42048589` Is there a way to use unstable modules from Rust stable?

Up until: :rev:`RB3163de5d298ce5df62d3f3e7e0d0ef9e2d1fc86f`


Week 26 (January 30)
====================

Info
----

This week I added multiple object support & edit-mode/object-mode switching,
with initial data structure for scene, object-layers and object-instances.

I ended up needing to define a custom linked list structure (which I'd have rather avoided),
but its needed so items in the list can be modified in-place - allowing low-level list manipulation
*(which Rust's API doesn't provide)*.

Also generalized keymap and UI code by defining ``OpAction``
so an action can be pre-defined with a general API, then passed to the UI or key-map definitions.


Next Week
---------

Continue to improve object-mode support (basic add/delete/selection operators),
and continue to improve low level data access API's.


BMesh-Rust
----------

- Set active layer when selecting objects
  :rev:`RBe769a78d993a2bda229dbd0a5b1f1b4042106600`
- Add mut/immutable versions of pick functions
  :rev:`RB8e23dcfee840f489dcb1ac9bc6ce2714c2634b3f`
- Object layer support
  :rev:`RB703bfc93a69fa07582d1611691bb511f7a727e6d`
- Object pointer selection support
  :rev:`RB2739b2b57dbd9c205c712e4896880bf8bda0151b`
- macro to set/clear a flag: ``assign_flag_from_bool``
  :rev:`RB06d5cbdc22bd017055dd6e05f6a30f2a4c5b9e6e`
- Support object-mode/editmode switching

  - Add scene and object-instances.
  - Add object-instance matrix support for drawing & selection.
  - Add object-mode drawing.
  - Add object/edit mode switching and conversion between BMesh/Mesh.

  :rev:`RB489e4083469dc5f1701af3bfcad3b04087d19166`
- Add ``list_base`` crate
  :rev:`RBba177396b10e34ee463cb11401bc088883125b31`
- plain_ptr: Add order impl
  :rev:`RBa51040d5814d3e459c0c8c692c22b794578decb9`
- Prefs: add object color
  :rev:`RB0cdbcff6035fd201c658a642eaf4795d039687e5`
- BMesh: add ``BMesh.clear()``
  :rev:`RB8e20a8d478c45b4ab8d9cc111d4d19adb4fbf6dd`
- Add utility to run all tests
  :rev:`RB5ae1b16f8ffac29186e84b678f8ca6df75cbacef`
- Initial MainData struct *(Use to store all file-data)*.
  :rev:`RBfa35f5403e052ed780e99916828ad09526a2b945`
- RNA: debug-mode sanity check setting enum value
  :rev:`RBdceeabf802b32b6d71c114f87683679e1b2286f5`
- RNA: Use ``OpAction`` when constructing UI or keymap actions
  *(This replaces awkward property callback in builders.)*.
  :rev:`RBa02ee6f2ce0972e2c5c881ecf52d70d0e16e0d1e`
- RNA: add ability to set enums by 'id'.
  :rev:`RBa77b65576d2640704a5b9aacba40ed35cd019e19`
- RNA: use pointer for PointerRNA data field
  :rev:`RB79d13c1b0a6e788de17dc9733c3520e8138977cf`


Questions
---------

.. hlist::
   :columns: 1

   - :qa:`42019529` How to clone/pull a git repository, ignoring LFS?
   - :qa:`41992247` Conventions for naming mutable/immutable API functions?
   - :qa:`41970758` How to recursively test all crates under a directory?
   - :qa:`41969714` Possible to use a macro to expand into a tuple of constructors?

Up until: :rev:`RBe769a78d993a2bda229dbd0a5b1f1b4042106600`


Week 25 (January 23)
====================

Info
----

This week I focused on creating a data API (RNA), for keymap and user interface access.

Made a utility for using a single struct member as a key in a set, (used for registrable types).

Also some other minor mesh editing operations.


Next Week
---------

Work on initial file-data (internal database, which will eventually be part of the file format).

Although there are still many other tasks relating to the data-api, tools and windowing,
so they may be some moving between tasks.


BMesh-Rust
----------

- RNA: use data API for operator properties and keymap
  :rev:`RB5a1a1900a3aedf5d6a70d92413c397e690b0eccb`
- app_engine: move defaults into their own file
  :rev:`RBdb2e69403c7e4e7f0fc147b1150f2b802892391d`
- RNA: use ``Cow`` for passing strings & slices
  :rev:`RBdfe34b7609ff7ff82daa383f23ef72966c333396`
- Initial Data API (RNA)
  :rev:`RB36bbbaf9ce2ee07d093dedc67ef7a3f084c7ba6b`
- Replace ``Vec`` with ``BTreeSet`` for registrable types
  :rev:`RB49216436cb1c88fc6a6d1d160086503778ca25d8`
- Macro for using a single member as a key in a set
  :rev:`RB7d1d3b5256d88473dcad3a99d1d5bd649dfbfe6c`
- Pointer Macros: add ``offset_of_enum``
  :rev:`RBdc1cb8177eef2f994d59cc42075caeb6ae3dfc4d`
- BMesh: ensure BMesh header is first in elem structs
  :rev:`RB2b45304e8804173b225f16ba65e65423175e8a93`
- BMesh: add hide-reveal operators
  :rev:`RB022c70029abc151c16e993192b7dbabe85bb87a0`

Questions
---------

.. hlist::
   :columns: 1

   - :qa:`41862596` Is it possible to coerce slices/vectors into ``Cow<[type]>`` in function arguments?
   - :qa:`41860110` How to return either a reference or newly created data?
   - :qa:`41823321` How to get pointer offset of an enum member in bytes?
   - :qa:`41817718` Get value from an enum when its known?

Up until: :rev:`RB5a1a1900a3aedf5d6a70d92413c397e690b0eccb`


Week 24 (January 16)
====================

Info
----

This week I worked on improving mesh drawing, triangle drawing
add mesh and delete operators & menu.

Also basic edge & face creation.

Next Week
---------

Data API for keymap and operator property access.

BMesh-Rust
----------

- BMesh: remove static lifetime requirement for closures
  :rev:`RB8ec4d77b823a9de7bfd8827a8bdacd973c21a5d7`
- BMesh: edge/face add operator *(only edges & triangles).*
  :rev:`RB6adea4c77d74b7b431543da6d81a683c54b7ad23`
- BMesh: hide API
  :rev:`RB564b76ad2b61f21b3191b1739cb9abf8a99a73cb`
- bitflag_macros: Add ``.set`` method
  :rev:`RBf19f1bbbd1850e8080e4e263a7f9daf91923240a`
- Mesh: Add ``bmesh::ops`` module *(Also use builder for delete operation).*
  :rev:`RB87e373bae7165854e319e65528621d93c2235516`
- BMesh: add initial queries test
  :rev:`RB181c3935c345119847c958726f5c2d69bacc4cb1`
- BMesh: add Debug to bmesh structs (for assertions)
  :rev:`RB3015ecea4f225989f814afd3cff6e4ce8176e6c0`
- BMesh: add ``mut`` variants of element iterators
  :rev:`RB9f676da03e1b30f616948bfc9f2f03b0e149ad39`
- BMesh: delete operator & menu
  :rev:`RBfe0189a21900deb6e33bdb2b4b7b513326440bca`
- View3D: draw selection in ordered passes *(unselected, then selected).*
  :rev:`RB72030e13b3a40ad82e21f70be4786676a571c63c`
- PxBuf Draw: alpha blending for triangle fill
  :rev:`RB892d4bacad8ef2d966ecf7a9aed325631ad254b9`
- PxBuf Draw: avoid overlap drawing adjacent triangles
  :rev:`RBd2c41d367de49d93ba0551ce046f69779a9a9e75`
- PxBuf Draw: ability to get a x-slice from an image
  :rev:`RB56ef93d42fdb66608c96aab24d57095283a510ab`
- PxBuf: Add pixel access functions
  :rev:`RB90ceda89aa5d4bee1ca5c5c5509feaac8b24325c`
- View3D: draw filled faces (alpha).
  :rev:`RB570297c81a06ee86c69ef90ea8f9cec27bec381f`
- Fix ``rgba_hex_u32`` & ``rgb_hex_u32``
  *(would fail to resolve method when input wasn't explicitly typed u32).*
  :rev:`RB3866de4fe7ad5f2b85d67a301e82ef977c70475b`
- PxBuf Draw: Triangles
  :rev:`RBab6e1b9ca320844385684359419cc5f22601ef93`
- Windowing: only clear the buffer in debug mode
  :rev:`RB81db8100470d662435a5aa9c84d2f529c71eafa1`
- PxBuf Draw: Expose unclipped line drawing
  *(Also slight efficiency improvement to rectangle drawing).*
  :rev:`RB9fc6953351ede1db0673c5a419f3f929649541fe`
- BMesh: add primitives (menu)
  :rev:`RBe05278ee0ee8fc2261023eafd7448cb6153b7358`
- BMesh: add ``BM_mesh_elem_hflag_*`` functions
  :rev:`RB7bce7891a67dfd8e5f34959508bfe514d4ce05f0`
- BMesh: Support iterating over generic elements
  :rev:`RB905495fb314732f49aa006c22cd0f0184e33e443`
- Add initial ``bmesh_editmesh`` crate
  :rev:`RBc6670ea0d2d178e8f0fae91d95ee416404d568b0`
- BMesh: fix bm_iter_loops_of_edge_radial, null check
  :rev:`RB2d1c6b4656d2ee31873e7f70e14b6c4ec1b063bd`

Questions
---------

.. hlist::
   :columns: 1

   - :qa:`41742046` Is there a list of all cfg features?
   - :qa:`41738049` Idiomatic way to store a closure for reuse?
   - :qa:`41705542` How to pass a blank / empty argument to a macro in Rust?
   - :qa:`41651700` How can code that uses C FFI in Rust keep in sync with headers?
   - :qa:`41651253` Possible to avoid repeating struct name in an 'impl' functions body?
   - :qa:`41648483` Vectors/arrays with a fixed maximum size as part of a struct in Rust?

Up until: :rev:`RB8ec4d77b823a9de7bfd8827a8bdacd973c21a5d7`


Week 23 (January 9)
===================

Info
----

This week I moved menus to use generic interface code, with initial nested tree structure for interface data
and an API for declaring interface elements using the builder-pattern.

Also added edge and face draw modes, with a menu for switching between modes.


Next Week
---------

Continue to improve interface and drawing and basic modeling.


BMesh-Rust
----------

- Add operator and menu for switching modes
  :rev:`RB71e267886d56cf7a6004e1d726429f21544e9343`
- Add edge and face selection & selection flush builder
  :rev:`RB23bead281c9562020bc34ec4bc74d8b08df1ccc6`
- Add distance-to-line functions
  :rev:`RBcaec3b271dedc0f4fd957557aa6c29524679362d`
- Use builder-pattern for define operator buttons
  :rev:`RBfef3c2b2ee4046fff7b5346db6c4fb54d75b1e65`
- Move menu's to generic user interface code
  :rev:`RBd4472f8bf4c79f84380f9219a0c886377bffb8ff`


Questions
---------

.. hlist::
   :columns: 1

   - :qa:`41648339` How to specify the underlying type of an enum in Rust?
   - :qa:`41637978` How to get the number of elements in an enum as a constant value?
   - :qa:`41629819` How to best *fake* keyword style function arguments in Rust?
   - :qa:`41617182` How to write an idiomatic build pattern with chained method calls in Rust?
   - :qa:`41573764` Can the ``let Some(var) = option;``
     syntax be used in a situation where it's known that it's not `None`?

Up until: :rev:`RB71e267886d56cf7a6004e1d726429f21544e9343`


Week 22 (January 2)
===================


Info
----

This week I spent time on font drawing, menu window type and their registration.


Next Week
---------

Initial user-interface code (buttons/widgets),
similar to window types, plug-ins should be able to register new button types at run-time.

For the purpose of getting something basic working - only labels and operator-buttons will be needed,
possibly add sub-menus too.


General Tasks
-------------

Made a bitmap to vector font converter for quick scaled text drawing,
this converts single pixel width BDF fonts to vector-edge arrays.

See: `BDF 2 CenterLine <https://gitlab.com/ideasman42/bdf2centerline>`__ project page.


BMesh-Rust
----------

- Use menu_types callback and register a menus
  :rev:`RBdd0e4e47ed1748de875a99918bc1b32f6e42bb66`
- Pass ``&str`` instead of ``&String`` for registered types
  :rev:`RB81da4a0d982291f0a3b42025b1ec3998b3d904e6`
- Use ``&str`` instead of ``Into<String>`` for property access
  :rev:`RB618ac5cfe1307961b4fdd9e032a3d3fcad2eb553`
- Use font drawing for menus
  :rev:`RB32b5c1b1499a0a56e253229ece367060fce48d29`
- Add basic font drawing,
  :rev:`RB3c536f63c70582a1a6004b01f163d24ca8f631bc`
- Menu can now be opened and clamps to window bounds
  :rev:`RBae4ac7f051cf51683b0f476ef60d07ab8bbf333e`
- Initial menu/popup window type, (also use ``plain_ptr`` for windowing).
  :rev:`RBb52360564e17e0ef2fe97f365a5c78be4a3a0bf9`


Questions
---------

.. hlist::
   :columns: 1

   - :qa:`41499789` Possible to pass a ``&str`` or ``&String`` to function in Rust using ``Into`` ?
   - :qa:`41469686` How to decode a single UTF-8 character and step onto the next using only the Rust standard library?
   - :qa:`41457368` How to check if ``Box<Any>`` contains 'Unit' in a short expression?
   - :qa:`41455206` Is it possible to decode bytes to UTF-8, converting errors to escape sequences in Rust?
   - :qa:`41449708` How to print a ``u8`` slice as text if I don't care about the particular encoding?
   - :qa:`41436525` How to avoid writing duplicate accessor functions for mut/const references in Rust?
   - :qa:`41421107` Borrow checker and function arguments in Rust, correct or over zealous?
   - :qa:`41410212` How to avoid repetition expanding indices with macros in Rust?

Up until: :rev:`RB18b1a624be6853edaada388eac5cd98a4ebcad7b`
