
import sys, os

sys.path.insert(0, os.path.abspath(os.path.join('..', 'exts')))

extensions = [
    'bl_rev',
    'bl_qa',
    # 'bl_diff',
    # 'bl_task',
]

# The suffix of source filenames.
source_suffix = '.rst'

exclude_patterns = ["template.rst"]
master_doc = 'index'

# General information about the project.
project = 'Weekly Done-List of ideasman42'
copyright = 'Creative Commons'

# without this it calls it 'documentation', which it's not
html_title = "weekly-donelist-ideasman42"
html_short_title = "ideasman42's weekly done-list"

# visual noise for my purpose
html_show_copyright = False
html_show_sphinx = False
html_show_sourcelink = False

'''
try:
    import sphinx_rtd_theme
except ImportError:
    sphinx_rtd_theme = None

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
if sphinx_rtd_theme:
    html_theme = 'sphinx_rtd_theme'
else:
    html_theme = 'haiku'

if sphinx_rtd_theme:
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
'''

html_theme = 'alabaster'
html_theme_options = {
    "show_powered_by": False,
    }
import alabaster
html_theme_path = [alabaster.get_path()]


# for our own extensions!
# Formatted: {callsign: (local_path, remote_path)}
# Where the local path is only used to validate sha1's, can be None.
bl_commit_bases = {
    # (R)ust simple (E)xamples:
    "RE": ("/src/rust-simple-examples/", "https://gitlab.com/ideasman42/rust-simple-examples/commit/"),
    # (R)aster Re(T)race:
    "RT": ("/src/retrace/", "https://gitlab.com/ideasman42/raster-retrace/commit/"),
    # (R)ust (B)mesh:
    "RB": ("/src/bmesh-rs/", "https://gitlab.com/ideasman42/bmesh-rs/commit/"),
}

bl_qa_base = "http://stackoverflow.com/"

