import sys
f = list(sys.stdin.readlines())

ind = []

for i, l in enumerate(f):
    if l.startswith("=") and set(l.strip()) == {"="}:
        ind.append(i - 1)
ind.append(i)

def pairs(iterable):
    it = iter(iterable)
    a = next(it)
    while True:
        b = next(it)
        yield a, b
        a = b

for a, b in reversed(list(pairs(ind))):
    print("".join(f[a:b]))

